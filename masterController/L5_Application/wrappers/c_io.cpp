/*
 * c_io.cpp
 *
 *  Created on: Apr 22, 2019
 *      Author: @Neeraj Dhavale
 */
#include "c_io.h"
#include "io.hpp"
#include <stdbool.h>

void led_set(uint8_t num,bool state){
    LE.set(num,state);
}


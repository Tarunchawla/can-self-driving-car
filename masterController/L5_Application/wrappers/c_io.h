/*
 * c_io.h
 *
 *  Created on: Apr 22, 2019
 *      Author: @Neeraj Dhavale
 */

#ifndef C_IO_H_
#define C_IO_H_
#include <stdbool.h>
#include "stdio.h"
#ifdef __cplusplus
extern "C" {
#endif

void led_set(uint8_t num, bool state);


#ifdef __cplusplus
}
#endif
#endif /* C_IO_H_ */

/*
 * Master_HRT_BEAT.h
 *
 *  Created on: Apr 13, 2019
 *      Author: @Neeraj Dhavale
 */

#ifndef MASTER_HRT_BEAT_H_
#define MASTER_HRT_BEAT_H_

#include "stdio.h"
#include "stdbool.h"
#include "utilities.h"


void send_heartbeat(void);

bool CAN_HW_init(void);

void rx_sensor_heartbeat(void);

#endif /* MASTER_HRT_BEAT_H_ */

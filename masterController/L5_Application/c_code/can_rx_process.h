/*
 * can_rx_process.h
 *
 *  Created on: Apr 16, 2019
 *      Author: @Neeraj Dhavale
 */

#ifndef CAN_RX_PROCESS_H_
#define CAN_RX_PROCESS_H_

#include "stdint.h"
#include "stdbool.h"

typedef struct{
    bool start;
    float steer_angle;
    uint16_t speed;
    uint8_t rx_left_threshold;
    uint8_t rx_right_threshold;
    uint8_t rx_front_threshold;
    bool reverse;
}MASTER_CONTROL_t;



void receive_can_msg(MASTER_CONTROL_t *master_control);



#endif /* CAN_RX_PROCESS_H_ */

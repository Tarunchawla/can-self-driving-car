/*
 * can_tx_processor.h
 *
 *  Created on: Apr 29, 2019
 *      Author: @Neeraj Dhavale
 */

#ifndef CAN_TX_PROCESSOR_H_
#define CAN_TX_PROCESSOR_H_

#include "can_rx_process.h"

void send_senor_threshold(MASTER_CONTROL_t *master_control);


#endif /* CAN_TX_PROCESSOR_H_ */

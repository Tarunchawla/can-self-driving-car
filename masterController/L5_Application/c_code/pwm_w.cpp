/*
 * pwm_w.cpp
 *
 *  Created on: Apr 23, 2019
 *      Author: @Neeraj Dhavale
 */
#include <c_code/pwm.h>
#include "pwm_w.h"

LabPwm test;

bool set_duty_for_servo_init(){
   test.PwmSelectPin(LabPwm::k2_5);
   test.PwmInitSingleEdgeMode(80);
   set_duty_for_servo(12);
    return true;
}


bool set_duty_for_servo(float duty_cycle_servo){

    test.PwmInitSingleEdgeMode(80);
    test.SetDutyCycle(LabPwm::k2_5, duty_cycle_servo);

    return true;
}



/*
 * motor_commands.c
 *
 *  Created on: Apr 13, 2019
 *      Author: @Neeraj Dhavale
 */

#include "motor_commands.h"
#include "../_can_dbc/generated_can.h"
#include "can.h"


bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8])
{
    can_msg_t can_msg = { 0 };
    can_msg.msg_id                = mid;
    can_msg.frame_fields.data_len = dlc;
    memcpy(can_msg.data.bytes, bytes, dlc);
    return CAN_tx(can1, &can_msg, 0);
}

void send_motor_speed(uint16_t speed){
    /*
     * for sending speed in mps
    MOTOR_SPEED_t motor_speed = {0};
    motor_speed.MOTOR_SPEED_speed_in_mps = speed;
    dbc_encode_and_send_MOTOR_SPEED(&motor_speed);
     */
    DEMO_MOTOR_SPEED_t motor_speed = {0};
    motor_speed.MOTOR_SPEED_DUTY_CYCLE = speed;
    dbc_encode_and_send_DEMO_MOTOR_SPEED(&motor_speed);

}
void send_steer_commands(float steer_angle){
    /*
     * for angle
    STEER_DIRECTION_t steer = {0};
    steer.STEER_DIRECTION_ANGLE = steer_angle;
    dbc_encode_and_send_DEMO_STEER_DIRECTION(&steer);
     */
    DEMO_STEER_DIRECTION_t steer = {0};
    steer.STEER_DUTY_CYCLE_VAL = steer_angle;
    dbc_encode_and_send_DEMO_STEER_DIRECTION(&steer);

}



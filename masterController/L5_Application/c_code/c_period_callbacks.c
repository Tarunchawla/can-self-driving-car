/**
 * @file
 *
 * The purpose of this "C" callbacks is to provide the code to be able
 * to call pure C functions and unit-test it in C test framework
 */

#include <stdint.h>
#include "stdio.h"
#include <stdbool.h>
#include "c_uart2.h"
#include "motor_commands.h"
#include "steer_processor.h"
#include "Master_HRT_BEAT.h"
#include "can_rx_process.h"
#include "can_tx_processor.h"
#include "pwm_w.h"



MASTER_CONTROL_t master_control ={0};

bool C_period_init(void) {
    bool init  = CAN_HW_init();
    set_duty_for_servo_init();
    return init;
}

bool C_period_reg_tlm(void) {
    return true;
}

void C_period_1Hz(uint32_t count) {
    (void) count;
    send_heartbeat();
    send_senor_threshold(&master_control);
}

void C_period_10Hz(uint32_t count) {
    (void)count;
    receive_can_msg(&master_control);
    if(!master_control.start){
        send_motor_speed(0);
        set_duty_for_servo(12);
        return;
    }
    send_motor_speed(master_control.speed);
    set_duty_for_servo(master_control.steer_angle);
    send_steer_commands(master_control.steer_angle);
}

void C_period_100Hz(uint32_t count) {
    (void) count;

}

void C_period_1000Hz(uint32_t count) {
    (void) count;
}



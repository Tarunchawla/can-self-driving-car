/*
 * pwm_w.h
 *
 *  Created on: Apr 23, 2019
 *      Author: @Neeraj Dhavale
 */

#ifndef PWM_W_H_
#define PWM_W_H_

#ifdef __cplusplus
extern "C" {
#endif


bool set_duty_for_servo_init();
bool set_duty_for_servo(float duty_cycle_servo);



#ifdef __cplusplus
}
#endif

#endif /* PWM_W_H_ */

/*
 * can_tx_processor.c
 *
 *  Created on: Apr 29, 2019
 *      Author: @Neeraj Dhavale
 */

#include "can_tx_processor.h"
#include "../_can_dbc/generated_can.h"
#include "can.h"
#include "string.h"

void send_senor_threshold(MASTER_CONTROL_t *master_control){
    DBG_SENSOR_THRESHOLD_t dbg_sensor_values = {0};

    dbg_sensor_values.THRESHOLD_VALUE_Front = master_control->rx_front_threshold;
    dbg_sensor_values.THRESHOLD_VALUE_Left = master_control->rx_left_threshold;
    dbg_sensor_values.THRESHOLD_VALUE_Right = master_control->rx_right_threshold;

    dbc_encode_and_send_DBG_SENSOR_THRESHOLD(&dbg_sensor_values);
}

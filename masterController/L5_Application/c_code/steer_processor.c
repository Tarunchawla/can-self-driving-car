/*
 * steer_processor.c
 *
 *  Created on: Apr 13, 2019
 *      Author: @Neeraj Dhavale
 */

#include "steer_processor.h"
#include "utilities.h"


uint8_t left_threshold = 50;
uint8_t right_threshold = 50;
uint8_t front_threshold = 10;
uint32_t counter;
int8_t speed = 100;
uint8_t result = 12,prev_result;

bool reverse,loop_lock = true;


float steer_processor(uint32_t sensor_left,uint32_t sensor_right,uint32_t sensor_front){

    (void)sensor_front;
//TODO: update threshold over dbc

    if(speed > 0){
        if(sensor_left < left_threshold)
            result = RIGHT;
        else if(sensor_right < right_threshold)
            result = LEFT;
        else
            result = STRAIGHT;
        prev_result = result;
    }

    if(speed < 0){
        if(prev_result == RIGHT)
            result = LEFT;
        else if(prev_result == LEFT)
            result = RIGHT;
        else
            result = STRAIGHT;
    }


    return result;
}

/*
 * Function to decide the speed of car
 */

uint16_t speed_controller(uint32_t sensor_left,uint32_t sensor_right,uint32_t sensor_front){
    (void)sensor_front;

    //TODO: update threshold over dbc
    if(sensor_front < front_threshold){
        speed = 0;
        delay_ms(10);
        speed = -100;
    }
    else if((sensor_left < left_threshold) && (sensor_right < right_threshold)){
        speed = 0;
        delay_ms(20);
        speed = -100;
    }
    else{
        speed = SPEED_LOW;
    }

    return speed;
}



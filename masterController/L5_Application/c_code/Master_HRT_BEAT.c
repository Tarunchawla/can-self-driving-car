/*
 * Master_HRT_BEAT.c
 *
 *  Created on: Apr 13, 2019
 *      Author: @Neeraj Dhavale
 */

#include "stdio.h"
#include "Master_HRT_BEAT.h"
#include "../_can_dbc/generated_can.h"
#include "can.h"
#include "string.h"



void send_heartbeat(void){
    MASTER_HEARTBEAT_t hrt_bt = {0};
    hrt_bt.HEART_BEAT = 0xFF;
    printf("HRT_BEAT sent\n");
    dbc_encode_and_send_MASTER_HEARTBEAT(&hrt_bt);
}

void rx_sensor_heartbeat(void){
    //    SENSOR_HEARTBEAT_t sensor = {0};
    //    can_msg_t can_msg = {0};
    //    dbc_msg_hdr_t can_msg_hdr;
    //
    //    while(CAN_rx(can1,&can_msg,0)){
    //        can_msg_hdr.dlc = can_msg.frame_fields.data_len;
    //        can_msg_hdr.mid = can_msg.msg_id;
    //        can_msg_hdr.dlc = can_msg.frame_fields.data_len;
    //        can_msg_hdr.mid = can_msg.msg_id;
    //        if(dbc_decode_SENSOR_HEARTBEAT(&sensor,can_msg.data.bytes,&can_msg_hdr)){
    //            printf("Sensor HRT_BEAT: %d",sensor.HEART_BEAT);
    //        }
    //    }
}

bool CAN_HW_init(void){
    bool init = CAN_init(can1,100, 100, 100, NULL,NULL);
    printf("INIT successful\n");

    const can_std_id_t *slist      = NULL;
    const can_std_grp_id_t sglist[] = { {CAN_gen_sid(can1, 600), CAN_gen_sid(can1, 800)}};
    const can_ext_id_t *elist       = NULL; // Not used, so set it to NULL
    const can_ext_grp_id_t *eglist = NULL;// Group 1

    CAN_setup_filter(slist, 0, sglist, 1,elist, 0, eglist, 0);

    //    CAN_bypass_filter_accept_all_msgs();
    CAN_reset_bus(can1);
    delay_ms(500);
    printf("Init Function\n");
    return init;
}

/*
 * can_rx_process.c
 *
 *  Created on: Apr 16, 2019
 *      Author: @Neeraj Dhavale
 */

#include "can_rx_process.h"
#include "../_can_dbc/generated_can.h"
#include "can.h"
#include "../wrappers/c_io.h"
#include "steer_processor.h"

APP_CMD_t app_CMD_packet = {0};
SONAR_SENSOR_VALUES_t sensor_packet ={0};
MOTOR_SPEED_t speed_packet = {0};
STEER_DIRECTION_t steer = {0};
SONAR_SENSOR_THRESHOLD_t sensor_threshold = {0};

//
const uint32_t APP_CMD__MIA_MS = 3000;
const APP_CMD_t APP_CMD__MIA_MSG = {0};
const uint32_t  DEMO_IR_SENSOR__MIA_MS = 3000;
const DEMO_IR_SENSOR_t DEMO_IR_SENSOR__MIA_MSG = {0};
const uint32_t SONAR_SENSOR_VALUES__MIA_MS = 3000;
const SONAR_SENSOR_VALUES_t SONAR_SENSOR_VALUES__MIA_MSG = {0};
const uint32_t SONAR_SENSOR_THRESHOLD__MIA_MS = 2000;
const SONAR_SENSOR_THRESHOLD_t SONAR_SENSOR_THRESHOLD__MIA_MSG = {0};

void start_stop_CMD(MASTER_CONTROL_t *master_control){
    if((app_CMD_packet.START_COMMAND == 0xFF) && (app_CMD_packet.ABORT_COMMAND == 0x00)){
        master_control->start = true;
    }
    else if((app_CMD_packet.START_COMMAND == 0x00) && (app_CMD_packet.ABORT_COMMAND == 0xFF)){
        master_control->start = false;
    }


    if(dbc_handle_mia_APP_CMD(&app_CMD_packet,10)){
        master_control->start = 1;
    }

}

void update_sensor_values(MASTER_CONTROL_t *master_control){
    master_control->steer_angle = steer_processor(sensor_packet.SONAR_SENSOR_VALUES_Left_Sensor,
            sensor_packet.SONAR_SENSOR_VALUES_Right_Sensor,
            sensor_packet.SONAR_SENSOR_VALUES_Front_Sensor);
    master_control->speed = speed_controller(sensor_packet.SONAR_SENSOR_VALUES_Left_Sensor,
            sensor_packet.SONAR_SENSOR_VALUES_Right_Sensor,
            sensor_packet.SONAR_SENSOR_VALUES_Front_Sensor);
}

void update_sensor_threshold(MASTER_CONTROL_t *master_control){
    master_control->rx_left_threshold = sensor_threshold.UPDATE_THRESHOLD_VALUE_Left;
    master_control->rx_right_threshold = sensor_threshold.UPDATE_THRESHOLD_VALUE_Right;
    master_control->rx_front_threshold = sensor_threshold.UPDATE_THRESHOLD_VALUE_Front;

}

void receive_can_msg(MASTER_CONTROL_t *master_control){

    //bridge
    can_msg_t can_msg = {0};
    while(CAN_rx(can1,&can_msg,0)){
        dbc_msg_hdr_t can_msg_hdr = {0};
        can_msg_hdr.dlc = can_msg.frame_fields.data_len;
        can_msg_hdr.mid = can_msg.msg_id;

        if(dbc_decode_APP_CMD(&app_CMD_packet,can_msg.data.bytes,&can_msg_hdr)){
            start_stop_CMD(master_control);
        }


        if(dbc_decode_SONAR_SENSOR_THRESHOLD(&sensor_threshold,can_msg.data.bytes,&can_msg_hdr)){
            update_sensor_threshold(master_control);
        }

        if(dbc_decode_SONAR_SENSOR_VALUES(&sensor_packet,can_msg.data.bytes,&can_msg_hdr)){
            update_sensor_values(master_control);

        }
    }
    //Handle MIA for sensor values
    if(dbc_handle_mia_SONAR_SENSOR_VALUES(&sensor_packet,10)){
        led_set(1,true);
        master_control->rx_front_threshold = 0;
    }

    if(dbc_handle_mia_APP_CMD(&app_CMD_packet,100)){
        led_set(2,true);
    }

    if(dbc_handle_mia_SONAR_SENSOR_THRESHOLD(&sensor_threshold,1000)){
        sensor_threshold.UPDATE_THRESHOLD_VALUE_Front = 20;
        sensor_threshold.UPDATE_THRESHOLD_VALUE_Left = 40;
        sensor_threshold.UPDATE_THRESHOLD_VALUE_Right = 40;
    }

}



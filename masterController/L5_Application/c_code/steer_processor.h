/*
 * steer_processor.h
 *
 *  Created on: Apr 13, 2019
 *      Author: @Neeraj Dhavale
 */

#ifndef STEER_PROCESSOR_H_
#define STEER_PROCESSOR_H_

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#define SPEED_FAST          50
#define SPEED_MEDIUM        40
#define SPEED_LOW           100

#define LEFT     9
#define STRAIGHT 12
#define RIGHT    14

//#define RIGHT_THRESHOLD  50
//#define LEFT_THRESHOLD   50
//#define FRONT_THRESHOLD  50


float  steer_processor(uint32_t sensor_left,uint32_t sensor_right,uint32_t sensor_front);
uint16_t speed_controller(uint32_t sensor_left,uint32_t sensor_right,uint32_t sensor_front);


#endif /* STEER_PROCESSOR_H_ */

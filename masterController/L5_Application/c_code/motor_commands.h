/*
 * motor_commands.h
 *
 *  Created on: Apr 13, 2019
 *      Author: @Neeraj Dhavale
 */

#ifndef MOTOR_COMMANDS_H_
#define MOTOR_COMMANDS_H_

#include "stdio.h"
#include "stdint.h"
#include "string.h"
#include "stdbool.h"

void send_motor_speed(uint16_t speed);
void send_steer_commands(float steer_angle);

#endif /* MOTOR_COMMANDS_H_ */

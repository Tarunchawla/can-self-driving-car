/**
 * @file
 *
 * The purpose of this "C" callbacks is to provide the code to be able
 * to call pure C functions and unit-test it in C test framework
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "can_init.h"
#include "c_io.h"
#include "c_uart3.h"
#include "can_rx.h"
#include "uart_rx.h"
#include "uart_tx.h"
#include "diagnostics.h"
#include "c_period_callbacks.h"

BRIDGE_MESSAGES bridge_messages = { 0 };

bool C_period_init(void) {
    uart3_init(115200); //Set Baud Rate Uart3
    init_can();
    ld_setNum(00);
    return true;
}

bool C_period_reg_tlm(void) {
    return true;
}

void C_period_1Hz(uint32_t count) {
    (void) count;
    tx_count_display();
}

void C_period_10Hz(uint32_t count) {
    (void) count;
    chk_switch_start_cmd();
    chk_switch_stop_cmd();
}

void C_period_100Hz(uint32_t count) {
    (void) count;
    chk_uart3_rx();
    chk_can_rx(&bridge_messages);
    update_can_msgs(&bridge_messages);
    uart_publish_all_msgs(&bridge_messages);
}

void C_period_1000Hz(uint32_t count) {
    (void) count;
}

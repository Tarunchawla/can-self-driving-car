/*
 * diagnostics.h
 *
 *  Created on: Apr 19, 2019
 *      Author: Sanket
 */

#ifndef DIAGNOSTICS_H_
#define DIAGNOSTICS_H_

void tx_count_display();
void rx_count_display();
void dropped_count_display();
void rx_wm_display();
void rx_wm_display();
void chk_switch_start_cmd();
void chk_switch_stop_cmd();
#endif /* DIAGNOSTICS_H_ */

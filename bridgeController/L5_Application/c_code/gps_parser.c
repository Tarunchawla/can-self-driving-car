/*
 * gps_parser.c
 *
 *  Created on: Apr 19, 2019
 *      Author: Sanket
 */

#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "can_tx.h"

float gps_dest[100];

//Destination Message format: <GPD:XXX.XXXXX|XXX.XXXXX>
void parse_gps_destination(char *msg){
    char delim[10] = "<GPD:>|";
    char * pch;
    pch = strtok (msg,delim);
    int k=0;
    while (pch != NULL)
    {
        gps_dest[k] = atof(pch);
        pch = strtok (NULL, delim);
        k++;
    }
    can_send_dest_gps(gps_dest[0],gps_dest[1]);
}


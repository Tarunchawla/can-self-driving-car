/**
 *  @file
 * This is a C header file that other C code can use to access UART2
 * driver that is written in C++
 *
 * Note that we have a C header file, but its implementation in C++
 * such that we can invoke C++ Uart driver
 *
 * The C Unit-test framework can use this header file to "Mock" the
 * UART2 API and carry out the tests.
 */
#ifndef UART3_C_H__
#define UART3_C_H__
#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

bool uart3_init(int baud1);
bool uart3_getchar(char *byte, uint32_t timeout_ms);
void uart3_send(char *data1);
#ifdef __cplusplus
}
#endif
#endif /* UART3_C_H__ */

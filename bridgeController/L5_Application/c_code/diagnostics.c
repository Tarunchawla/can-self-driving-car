/*
 * diagnostics.c
 *
 *  Created on: Apr 19, 2019
 *      Author: Sanket
 */

#include "stdbool.h"
#include <stdio.h>
#include <stdlib.h>
#include "can.h"
#include "can_init.h"
#include "can_tx.h"
#include "uart_tx.h"
#include "c_io.h"
uint32_t counter_100ms =0;

void tx_count_display(){
    uint16_t tx_count = CAN_get_tx_count(CAN_NUM);
    ld_setNum(tx_count);
}

void rx_count_display(){
    uint16_t rx_count = CAN_get_rx_count(CAN_NUM);
    ld_setNum(rx_count);
}

void dropped_count_display(){
    uint16_t d_count = CAN_get_rx_dropped_count(CAN_NUM);
    ld_setNum(d_count);
}

void rx_wm_display(){
    uint16_t rw_count = CAN_get_rx_watermark(CAN_NUM);
    ld_setNum(rw_count);
}

void tx_wm_display(){
    uint16_t tw_count = CAN_get_tx_watermark(CAN_NUM);
    ld_setNum(tw_count);
}

void chk_switch_start_cmd(){

    if(sw_get(1)){
        if(counter_100ms >= 3){// Switch Debounce
            can_start_cmd();
            send_uart_message("Start Command Executed");
            counter_100ms=0;
        }
    }
    counter_100ms++;
}

void chk_switch_stop_cmd(){
    if(sw_get(2)){
        if(counter_100ms >= 3){// Switch Debounce
        can_stop_cmd();
        send_uart_message("Stop Command Executed");
        counter_100ms=0;
        }
    }
    counter_100ms++;
}

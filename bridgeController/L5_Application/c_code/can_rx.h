/*
 * can_rx.h
 *
 *  Created on: Apr 19, 2019
 *      Author: Sanket
 */

#ifndef CAN_RX_H_
#define CAN_RX_H_

#include "stdint.h"
#include "stdbool.h"

typedef struct{
    uint8_t master_heart_beat;
    float current_gps_x;
    float current_gps_y;
    uint8_t dist_dest;
    float steer_direction;
    float motor_speed;

}BRIDGE_MESSAGES;

void chk_can_rx(BRIDGE_MESSAGES *bridge_messages);
void update_can_msgs(BRIDGE_MESSAGES *bridge_messages);

#endif /* CAN_RX_H_ */

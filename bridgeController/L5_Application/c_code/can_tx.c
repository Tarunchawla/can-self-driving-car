/*
 * can_tx.c
 *
 *  Created on: Apr 19, 2019
 *      Author: Sanket
 */
#include "can.h"
#include "can_init.h"
#include "stdbool.h"
#include "stdlib.h"
#include "stdio.h"
#include "_can_dbc/generated_can.h"
#include "string.h"
#include "c_io.h"



bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8])
{
    can_msg_t can_msg = { 0 };
    can_msg.msg_id                = mid;
    can_msg.frame_fields.data_len = dlc;
    memcpy(can_msg.data.bytes, bytes, dlc);
    return CAN_tx(CAN_NUM, &can_msg, 0);
}

bool can_start_cmd()
{
    APP_CMD_t cmd;
    cmd.START_COMMAND = 0xFF;
    cmd.ABORT_COMMAND = 0x00;
    bool tx_ack = dbc_encode_and_send_APP_CMD(&cmd );
    return tx_ack;
}

bool can_stop_cmd()
{
    APP_CMD_t cmd;
    cmd.START_COMMAND = 0x00;
    cmd.ABORT_COMMAND = 0xFF;
    bool tx_ack = dbc_encode_and_send_APP_CMD(&cmd );
    return tx_ack;
}

bool can_send_dest_gps(float gps_dest_x,float gps_dest_y){
    APP_DESTINATION_GPS_t dest_gps;
    dest_gps.DEST_GPS_COORDINATES_X = gps_dest_x;
    dest_gps.DEST_GPS_COORDINATES_Y = gps_dest_y;
    bool tx_ack = dbc_encode_and_send_APP_DESTINATION_GPS(&dest_gps );
    return tx_ack;
}

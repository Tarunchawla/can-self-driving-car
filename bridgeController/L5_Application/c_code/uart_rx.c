/*
 *  uart_rx.c
 *
 *  Created on: Apr 19, 2019
 *      Author: Sanket
 */
#include "stdbool.h"
#include "stdlib.h"
#include "stdio.h"
#include "c_uart3.h"
#include "string.h"
#include "can_tx.h"
#include "gps_parser.h"

void chk_uart3_rx(){
    char byte = 0;


    if (uart3_getchar(&byte, 0)) {
        int i =0;
        char rxmsg[100] ="";
        rxmsg[i]=byte;
        while(uart3_getchar(&byte, 0)){
            i++;
            rxmsg[i]= byte;
        }
        //printf("UART Rx3 Data: %s\n",rxmsg);
        char temp[100] ="";
        strncpy(temp, rxmsg, 5);
        if(strcmp(rxmsg, "<GO1>")==0){
            can_start_cmd();
        }else if(strcmp(rxmsg, "<GO0>")==0){
            can_stop_cmd();
        }else if(strcmp(temp, "<GPD:")==0){
            parse_gps_destination(rxmsg);
        }
    }
}



#include "c_uart3.h"
#include "uart3.hpp"
#include "stdbool.h"
#include <stdio.h>
#include <stdlib.h>

bool uart3_init(int baud1){
    Uart3& u3 = Uart3::getInstance();
    bool init_status = u3.init(baud1);
    return init_status;
}

bool uart3_getchar(char *byte, uint32_t timeout_ms) {
    if(Uart3::getInstance().getChar(byte, timeout_ms))
        return true;
    else return false;
}

void uart3_send( char *data1) {
    Uart3& u3 = Uart3::getInstance();
    u3.putline(data1,1);
}

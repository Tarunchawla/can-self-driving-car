/*
 * c_can.h
 *
 *  Created on: Apr 19, 2019
 *      Author: Sanket
 */

#ifndef CAN_INIT_H_
#define CAN_INIT_H_
#include "stdint.h"
#include "stdbool.h"

#define BAUD_RATE 100
#define RXQ_SIZE 100
#define TXQ_SIZE 100
#define CAN_NUM can1

bool init_can(void);

#endif /* CAN_INIT_H_ */

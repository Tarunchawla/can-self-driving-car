/*
 * gps_parser.h
 *
 *  Created on: Apr 19, 2019
 *      Author: Sanket
 */

#ifndef GPS_PARSER_H_
#define GPS_PARSER_H_

void parse_gps_destination(char *msg);

#endif /* GPS_PARSER_H_ */

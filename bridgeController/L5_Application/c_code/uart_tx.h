/*
 * uart_tx.h
 *
 *  Created on: Apr 19, 2019
 *      Author: Sanket
 */

#ifndef UART_TX_H_
#define UART_TX_H_
#include "stdbool.h"
#include "can_rx.h"
void send_uart_heart_beat(void);
void send_uart_current_gps(float current_gps_x, float current_gps_y ,uint8_t dist_dest);
void send_uart_steer_direction(float steer_duty_cycle);
void send_uart_motor_speed(float motor_speed);
void send_uart_message(char* uart_message);
void uart_publish_all_msgs(BRIDGE_MESSAGES *bridge_messages);
#endif /* UART_TX_H_ */

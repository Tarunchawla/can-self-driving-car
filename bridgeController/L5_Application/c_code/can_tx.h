/*
 * can_tx.h
 *
 *  Created on: Apr 19, 2019
 *      Author: Sanket
 */

#ifndef CAN_TX_H_
#define CAN_TX_H_
#include "stdbool.h"

bool can_start_cmd(void);
bool can_stop_cmd(void);
bool can_send_dest_gps(float gps_dest_x,float gps_dest_y);

#endif /* CAN_TX_H_ */

#ifndef C_IO_H_
#define C_IO_H_

#ifdef __cplusplus
extern "C" {
#endif
#include"stdbool.h"

void ld_setNum(int num);
void le_set(int led_num, bool state);
bool sw_get(int sw_num);

#ifdef __cplusplus
}
#endif

#endif /* C_IO_H_ */

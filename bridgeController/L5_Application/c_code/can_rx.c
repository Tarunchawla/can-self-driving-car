/*
 * can_rx.c
 *
 *  Created on: Apr 19, 2019
 *      Author: Sanket
 */
#include "stdbool.h"
#include <stdio.h>
#include <stdlib.h>
#include "can.h"
#include "can_init.h"
#include "_can_dbc/generated_can.h"
#include "uart_tx.h"
#include "c_io.h"
#include "can_rx.h"

MASTER_HEARTBEAT_t master_hb = { 0 };
TELEMETRY_t telemetry = { 0 };
DEMO_STEER_DIRECTION_t steer_dir = { 0 };
DEMO_MOTOR_SPEED_t motor_spd = { 0 };
const uint32_t            MASTER_HEARTBEAT__MIA_MS = 2000;
const MASTER_HEARTBEAT_t  MASTER_HEARTBEAT__MIA_MSG = { 0 };
const uint32_t            TELEMETRY__MIA_MS = 2000;
const TELEMETRY_t  TELEMETRY__MIA_MSG = { 0 };
const uint32_t            DEMO_STEER_DIRECTION__MIA_MS = 2000;
const DEMO_STEER_DIRECTION_t  DEMO_STEER_DIRECTION__MIA_MSG = { 0 };
const uint32_t            DEMO_MOTOR_SPEED__MIA_MS = 2000;
const DEMO_MOTOR_SPEED_t  DEMO_MOTOR_SPEED__MIA_MSG = { 0 };

//CAN DBC Decode
void chk_can_rx(BRIDGE_MESSAGES *bridge_messages){
    can_msg_t can_msg = {0};
    while (CAN_rx(CAN_NUM,&can_msg, 0)){
        dbc_msg_hdr_t can_msg_hdr = {0};
        can_msg_hdr.dlc = can_msg.frame_fields.data_len;
        can_msg_hdr.mid = can_msg.msg_id;

        if(dbc_decode_MASTER_HEARTBEAT(&master_hb, can_msg.data.bytes, &can_msg_hdr)){
            le_set(1,0);
        }
        if(dbc_decode_TELEMETRY(&telemetry, can_msg.data.bytes, &can_msg_hdr)){
            le_set(2,0);
        }
        if(dbc_decode_DEMO_STEER_DIRECTION(&steer_dir,can_msg.data.bytes, &can_msg_hdr)){
            le_set(3,0);
        }
        if(dbc_decode_DEMO_MOTOR_SPEED(&motor_spd,can_msg.data.bytes, &can_msg_hdr)){
            le_set(4,0);
        }
//MIA
    }
    if(dbc_handle_mia_MASTER_HEARTBEAT(&master_hb, 10)){
        le_set(1,1);
    }
    if(dbc_handle_mia_TELEMETRY(&telemetry, 10)){
        le_set(2,1);
    }
    if(dbc_handle_mia_DEMO_STEER_DIRECTION(&steer_dir, 10)){
        le_set(3,1);
    }
    if(dbc_handle_mia_DEMO_MOTOR_SPEED(&motor_spd, 10)){
        le_set(4,1);
    }
}

//Update all bridge messages
void update_can_msgs(BRIDGE_MESSAGES *bridge_messages){
    bridge_messages->current_gps_x = telemetry.CURRENT_GPS_COORDINATES_X;
    bridge_messages->current_gps_y = telemetry.CURRENT_GPS_COORDINATES_Y;
    bridge_messages->dist_dest = telemetry.DISTANCE_TO_DEST;
    bridge_messages->steer_direction = steer_dir.STEER_DUTY_CYCLE_VAL;
    bridge_messages->motor_speed = motor_spd.MOTOR_SPEED_DUTY_CYCLE;
    bridge_messages->master_heart_beat = master_hb.HEART_BEAT;
}

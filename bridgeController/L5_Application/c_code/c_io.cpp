#include "c_io.h"
#include "io.hpp"


void ld_setNum(int num){
    LD.setNumber(num);
}

void le_set(int led_num, bool state){
    LE.set(led_num,state);
}

bool sw_get(int sw_num){
    bool sw_state = SW.getSwitch(sw_num);
    return sw_state;
}


/*
 * uart_tx.c
 *
 *  Created on: Apr 19, 2019
 *      Author: Sanket
 */
#include "stdlib.h"
#include "stdio.h"
#include "c_uart3.h"
#include "can_rx.h"
#include "utilities.h"
#include "c_period_callbacks.h"

uint32_t hb_count =0;
int case_sel = 0;
uint32_t ten_ms_counter = 0;

void send_uart_heart_beat(){
    hb_count++;
    char txmsg[100]= "";
    sprintf(txmsg, "<MHB:%lu>", hb_count);
    uart3_send(txmsg);
}

void send_uart_current_gps(float current_gps_x, float current_gps_y ,uint8_t dist_dest){
    char txmsg[100]= "";
    sprintf(txmsg, "<GPC:%f|%f|%u>",current_gps_x,current_gps_y,dist_dest );
    uart3_send(txmsg);
}

void send_uart_steer_direction(float steer_dir){
    char txmsg[100]= "";
    sprintf(txmsg, "<STD:%f>",steer_dir );
    uart3_send(txmsg);

}

void send_uart_motor_speed(float motor_speed){
    char txmsg[100]= "";
    sprintf(txmsg, "<MTS:%f>",motor_speed );
    uart3_send(txmsg);
}

void send_uart_message(char* uart_message){
    char txmsg[100]= "";
    sprintf(txmsg, "<%s>",uart_message );
    uart3_send(txmsg);
}

//Send all messages every 2 sec with 60ms delay between each msg
//to facilitate reliable MQTT transmission.
void uart_publish_all_msgs(BRIDGE_MESSAGES *bridge_messages){
    if(ten_ms_counter > 200){
        if(ten_ms_counter % 6 ==0){
            case_sel++;
            switch(case_sel){
                case 1: send_uart_motor_speed(bridge_messages->motor_speed);
                break;
                case 2: send_uart_steer_direction(bridge_messages->steer_direction);
                break;
                case 3: send_uart_current_gps(bridge_messages->current_gps_x,
                        bridge_messages->current_gps_y,
                        bridge_messages->dist_dest);
                break;
                case 4: if(bridge_messages->master_heart_beat==0xFF){
                    send_uart_heart_beat();
                }
                break;
                default: case_sel=0;
                ten_ms_counter =0;
                break;
            }
        }
    }
    ten_ms_counter++;
}

/*
 *  c_can.c
 *
 *  Created on: Apr 19, 2019
 *      Author: Sanket
 */
#include "can_init.h"
#include "stdbool.h"
#include <stdio.h>
#include <stdlib.h>
#include "can.h"
#include "utilities.h"

bool init_can(){
    bool init_status = CAN_init(CAN_NUM, BAUD_RATE, RXQ_SIZE, TXQ_SIZE, NULL, NULL);
    CAN_bypass_filter_accept_all_msgs();
    CAN_reset_bus(CAN_NUM);
    delay_ms(500);
    return init_status;
}

package com.cmpe243.mysterymachine;

import android.content.Context;
import android.util.Log;


import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.*;

import java.io.UnsupportedEncodingException;

import static android.support.constraint.Constraints.TAG;

public class MqttConnHelper {
    private Context context;
    private static final String ec2_server_url = "tcp://ec2-13-59-175-31.us-east-2.compute.amazonaws.com:1883";
    private static final String direct_wifi_ip = "tcp://192.168.4.1:1883";
    //private static final String ec2_server_url = "tcp://13.59.175.31:1883";

    private static int qos = 0;
    private static final String username = "";
    private static final String password = "";
    MqttConnectOptions mqttConnectOptions;

    public MqttAndroidClient mqttAndroidClient;
    public MqttConnHelper(Context context){
        this.context = context;
        MqttInitialize();
    }
    public void MqttInitialize(){
        String clientId = MqttClient.generateClientId();
        mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setCleanSession(false);
        //mqttConnectOptions.setUserName(username);
        //mqttConnectOptions.setPassword(password.toCharArray());
        mqttAndroidClient = new MqttAndroidClient(context, direct_wifi_ip /*ec2_server_url*/,clientId);
    }
    public void setCallback(MqttCallbackExtended callback) {
        mqttAndroidClient.setCallback(callback);
    }
    public boolean isConnected(){
        return mqttAndroidClient.isConnected();
    }

    public void MqttConnect(){
        try {
            IMqttToken token = mqttAndroidClient.connect(mqttConnectOptions, null,new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // We are connected
                    Log.i(TAG, "onSuccess: Connected to mqtt server");
                    DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
                    disconnectedBufferOptions.setBufferEnabled(true);
                    disconnectedBufferOptions.setBufferSize(100);
                    disconnectedBufferOptions.setPersistBuffer(false);
                    disconnectedBufferOptions.setDeleteOldestMessages(false);
                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions);
                }
                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    // Something went wrong e.g. connection timeout or firewall problems
                    Log.i(TAG, "onFailure: Connection failed", exception);
                }
            });
            Log.i(TAG, "MqttConnect: token:"+token);
        } catch (MqttException e){
            Log.e(TAG, "MqttConnect: Connection to mqtt server failed", e.getCause() );
        }
    }

    public void MqttPublishMessage(String msg, int qos, String topic)
            throws MqttException, UnsupportedEncodingException {
        byte[] encodedPayload = new byte[0];
        encodedPayload = msg.getBytes("UTF-8");
        MqttMessage message = new MqttMessage(encodedPayload);
        message.setId(5866);
        message.setRetained(true);
        message.setQos(qos);
        mqttAndroidClient.publish(topic, message);
    }

    public void MqttDisconnect(){
        try {
            mqttAndroidClient.disconnect().setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // We are disconnected
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    // Something went wrong e.g. connection timeout or firewall problems
                }
            });
        } catch(Exception e){
            Log.e(TAG, "MqttDisconnect: Disconnection from mqtt broker failed",e);
        }
    }

    public void MqttSubscribe(final String topic, int qos, IMqttMessageListener messageListener) throws MqttException {
        Log.i(TAG, "subscribe: going to subscribe");
        mqttAndroidClient.subscribe(topic, qos, messageListener);
        Log.i(TAG, "subscribe: subscribe method called");
    }

    public void MqttUnsubscribe(String topic){
        IMqttToken unSubToken;
        try {
            unSubToken = mqttAndroidClient.unsubscribe(topic);
        }catch(Exception e){
            Log.e(TAG, "MqttUnsubscribe: Unsubscribtion failed", e);
            return;
        }
        unSubToken.setActionCallback(new IMqttActionListener() {
            @Override
            public void onSuccess(IMqttToken asyncActionToken) {
            }
            @Override
            public void onFailure(IMqttToken asyncActionToken,
                                  Throwable exception) {
            }
        });
    }
}

package com.cmpe243.mysterymachine;

import android.content.Context;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import static android.content.ContentValues.TAG;

/**
 * A placeholder fragment containing a simple view.
 */
public class DisplayFragment extends Fragment {

    private MqttConnHelper connHelper;
    public Context context;
    private TextView cloudConnectionTextView;
    private TextView carConnectionTextView;
    private TextView compassTextView;
    private TextView sensorTextView;
    private TextView speedTextView;
    private TextView rpsTextView;
    private Button startButton;
    private Button stopButton;
    private static final String receiveTopic = "TX";
    private static final String publishTopic = "RX";
    private static final String wifiHeartbeatTopic = "WHB";
    private static final String masterHeartbeatTopic = "MHB";
    private static final String startCommand = "<GO1>";
    private static final String stopCommand = "<GO0>";
    private static final String connectedText = "Connected " + "\ud83d\udcf6";
    private static final String disconnectedText = "Disconnected " +"\ud83d\udeab";
    private static int qos = 1;
    private Handler handler;
    private boolean wifiHeartbeat = false;
    private boolean masterHeartbeat = false;
    private IMqttMessageListener wifiHeartbeatListener, masterHeartbeatListener, messageListener;

    public DisplayFragment() {
        handler = new Handler();
        wifiHeartbeatListener = new IMqttMessageListener() {

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                wifiHeartbeat = true;
            }
        };
        masterHeartbeatListener = new IMqttMessageListener() {
            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                masterHeartbeat = true;
            }
        };
        messageListener = new IMqttMessageListener() {
            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                Log.i(TAG, "messageArrived: topic:" + topic + " message:" + message.toString());
                String msg = message.toString();
                msg = msg.substring(1, msg.indexOf('>'));
                String subStrings[] = msg.split(":");
                if (subStrings[0].contains("CMP")) {
                    setCompassValue(subStrings[1]);
                } else if (subStrings[0].contains("SNR")) {
                    setSensorValue(subStrings[1]);
                } else if (subStrings[0].contains("RPS")) {
                    setRPSValue(subStrings[1]);
                } else if (subStrings[0].contains("SPD")) {
                    setSpeedValue(subStrings[1]);
                }
            }
        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
         View view = inflater.inflate(R.layout.fragment_display, container, false);
         cloudConnectionTextView = view.findViewById(R.id.cloudConnectionTextview);
         carConnectionTextView = view.findViewById(R.id.carConnectionTextview);
         compassTextView = view.findViewById(R.id.compassTextView);
         sensorTextView = view.findViewById(R.id.sensorTextView);
         speedTextView = view.findViewById(R.id.speedTextView);
         rpsTextView = view.findViewById(R.id.rpsTextView);
         startButton = view.findViewById(R.id.startButton);
         stopButton = view.findViewById(R.id.stopButton);
         startButton.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 try {
                     connHelper.MqttPublishMessage(startCommand, qos, publishTopic);
                 }catch (Exception e){
                     Log.w(TAG, "onClick: error publishing", e);
                 }
             }
         });
         stopButton.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 try {
                     connHelper.MqttPublishMessage(stopCommand, qos, publishTopic);
                 }catch (Exception e){
                     Log.w(TAG, "onClick: error publishing", e);
                 }
             }
         });
         Runnable runnable = new Runnable() {
             @Override
             public void run() {
                 if(wifiHeartbeat || masterHeartbeat) {
                     carConnectionTextView.setText("Car: " + connectedText);
                 } else {
                     carConnectionTextView.setText("Car: " + disconnectedText);
                 }
                 masterHeartbeat = false;
                 wifiHeartbeat = false;
                 handler.postDelayed(this, 3000);
             }
         };
         handler.post(runnable);
         return view;
    }

    @Override
    public void onAttach(final Context context){
        super.onAttach(context);
        this.context = context;
        connHelper = new MqttConnHelper(context);
        connHelper.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {
                cloudConnectionTextView.setText("Cloud: " + connectedText);
                try {
                    connHelper.MqttSubscribe(wifiHeartbeatTopic, qos, wifiHeartbeatListener);
                    connHelper.MqttSubscribe(masterHeartbeatTopic, qos, masterHeartbeatListener);
                    connHelper.MqttSubscribe(receiveTopic, qos, messageListener);
                } catch(Exception e){
                    Log.w(TAG, "connectComplete: subscription failed",e);
                }
            }
            @Override
            public void connectionLost(Throwable cause) {
                cloudConnectionTextView.setText("Cloud: " + disconnectedText);
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                //textView.setText(message.toString());
                if(topic == wifiHeartbeatTopic){
                    wifiHeartbeat = true;
                } else if(topic == masterHeartbeatTopic){
                    masterHeartbeat = true;
                }
                Log.d(TAG, "messageArrived: Received topic:"+ topic + "message:"+message.toString());
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {
                makeToast("Message sent");
            }
        });
        connHelper.MqttConnect();
    }
    void setCompassValue(String value){
        compassTextView.setText("COMPASS: " + value + "");
    }

    void setSensorValue(String value){
        sensorTextView.setText("SENSOR VALUES(L|F|R): " + value);
    }

     void setSpeedValue(String value){
        speedTextView.setText("Speed: " + value);
    }

     void setRPSValue(String value){
        rpsTextView.setText("RPS: " + value);
    }
    void makeToast(String message){
        Toast toast = Toast.makeText(getContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        connHelper.mqttAndroidClient.unregisterResources();
        connHelper.mqttAndroidClient.close();
    }
}

package com.cmpe243.mysterymachine;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttMessage;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MapNavigationFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MapNavigationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapNavigationFragment extends Fragment implements OnMapReadyCallback {

    private OnFragmentInteractionListener mListener;
    private static final int MM_FINE_LOCATION_PERMISSION = 0;
    private String TAG = "MysMac MapNavigationFragment";
    private String gpsMessage;
    private FloatingActionButton sendCoordinatesButton;

    private GoogleMap mMap;
    private Location mLastKnownLocation;
    private Marker mDestinationMarker;
    private boolean mLocationPermissionGranted;
    private boolean mqttBrokerConneted = false;
    private MqttConnHelper  mqttConnHelper;

    // Default location (SJSU) in case we are unable to get the location
    private final LatLng mDefaultLocation = new LatLng(37.336175, -121.881840);
    private static final int DEFAULT_ZOOM = 15;

    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;

    static  LatLng mCurrentLocation = new LatLng(-31.90, 115.86);

    public MapNavigationFragment() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment MapNavigationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MapNavigationFragment newInstance(MqttConnHelper connHelper) {
        MapNavigationFragment fragment = new MapNavigationFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this.getContext());
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map_navigation, container, false);
        sendCoordinatesButton = getActivity().findViewById(R.id.sendCoordinatesButton);
        sendCoordinatesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 gpsMessage = "<GPC:" + String.valueOf(mDestinationMarker.getPosition().latitude)
                        + "|" + String.valueOf(mDestinationMarker.getPosition().longitude) +"|0>";
                Log.d(TAG, "onClick: sending coordinate " + gpsMessage);
                if(mqttBrokerConneted) {
                    Log.d(TAG, "onClick: trying to send coordinates: " + gpsMessage);
                    try {
                        mqttConnHelper.MqttPublishMessage(gpsMessage, 0, "RX");
                    } catch (Exception e) {
                        Log.w(TAG, "onClick: publish failed", e);
                    }
                }
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mqttConnHelper = new MqttConnHelper(this.getContext());
        mqttConnHelper.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {
                mqttBrokerConneted = true;
            }
            @Override
            public void connectionLost(Throwable cause) {
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                //textView.setText(message.toString());
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {
                makeToast("Message sent");
            }
        });
        mqttConnHelper.MqttConnect();
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mDestinationMarker = mMap.addMarker(new MarkerOptions()
                .position(mDefaultLocation).draggable(true)
                .title("Destination"));
        mDestinationMarker.setVisible(true);
        checkMapPermission();

        Log.i(TAG, "onMapReady: adding locaiton on map");
        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
        private void checkMapPermission(){
        if (ContextCompat.checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this.getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                Toast.makeText(this.getContext(),"App need to access current location to plot the route", Toast.LENGTH_LONG);
            } else {
                ActivityCompat.requestPermissions(this.getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MM_FINE_LOCATION_PERMISSION
                        );
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MM_FINE_LOCATION_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                    getDeviceLocation();
                } else {
                    Toast.makeText(this.getContext(), "Cannot automatically get start location", Toast.LENGTH_LONG);
                    mLocationPermissionGranted = false;
                }
                return;
            }
        }
    }
    private void getDeviceLocation() {
        try {
            if (mLocationPermissionGranted) {
                Task locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this.getActivity(), new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.getResult();
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(mLastKnownLocation.getLatitude(),
                                            mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                            updateCurrentLocationMarker();
                        } else {
                            Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                            mMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
        } catch(SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private void updateCurrentLocationMarker(){
        mCurrentLocation = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
        Marker mCurrentPositionMarker = mMap.addMarker(new MarkerOptions()
                .position(mCurrentLocation)
                .draggable(true));
        Marker mDefaultPositionMarker = mMap.addMarker(new MarkerOptions()
                .position(mDefaultLocation));
    }
    void makeToast(String message){
        Toast toast = Toast.makeText(getContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

}

package com.cmpe243.mysterymachine;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

public class Display extends AppCompatActivity implements MapNavigationFragment.OnFragmentInteractionListener {

    DisplayFragment displayFragment;
    MapNavigationFragment mapNavigationFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        displayFragment = new DisplayFragment();
        mapNavigationFragment = new MapNavigationFragment();
        setDisplayFragment();
        setContentView(R.layout.activity_display);
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.action_info:
                        setDisplayFragment();
                        break;
                    case R.id.action_map:
                        setMapNavigationFragment();
                        break;
                    default:
                            break;
                }
                return false;
            }
        });
    }

    private void setDisplayFragment(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.fragment_container, displayFragment);
        fragmentTransaction.commit();
    }

    private void setMapNavigationFragment(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.fragment_container, mapNavigationFragment);
        fragmentTransaction.commit();
    }
    @Override
    public void onFragmentInteraction(Uri uri) {
        return;
    }
}

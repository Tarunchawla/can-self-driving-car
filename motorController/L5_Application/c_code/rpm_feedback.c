
#include <c_code/can_rx_tx.h>
#include <c_code/can_rx_tx.h>
#include <stdint.h>
#include <stdio.h>
#include "../_can_dbc/generated_can.h"
#include "printf_lib.h"
#include "pwm_wrapper.h"
#include "io_wrapper.h"
#include "rpm_feedback.h"
#include "eint.h"
#include "math.h"

uint32_t spoke_count;
uint32_t last_spoke_count=0;

void spoke_counter(){
    spoke_count++;
}

void feedback_init(){
    set_as_input();
    eint3_enable_port2(6,eint_falling_edge,spoke_counter);
}
void get_spd_cm_per_sec(CONTROL_t *control){
    float diff =(spoke_count-last_spoke_count);
    control->rps=diff/8;
    control->spd_cmps=control->rps*M_PI*10;
    last_spoke_count = spoke_count;
    can_app_send((uint8_t) control->spd_cmps);
    printf("spoke_count = %lu\n",spoke_count);
    printf("last_spoke_count = %lu\n",last_spoke_count);
    printf("RPS = %f\n",control->rps);
    printf("CMPS = %f\n",control->spd_cmps);
    printf("control->spd_cmp10ms = %f\n",control->spd_cmp10ms);
}

void get_spd_cm_ten_ms(CONTROL_t *control){
    float diff =(spoke_count-last_spoke_count);
    float rotations_per_10ms = diff/8;
    control->spd_cmp10ms = rotations_per_10ms * M_PI*10;
//    printf("control->spd_cmp10ms = %f\n",control->spd_cmp10ms);
}

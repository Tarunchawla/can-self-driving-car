/*
 * controller.c
 *
 *  Created on: Apr 26, 2019
 *      Author: Tarun
 */
#include "controller.h"
#include "stdio.h"
#include "can_rx_tx.h"
#include "pwm_wrapper.h"
#include "io_wrapper.h"
#define PWM_freq 4000
uint32_t calls =0;
uint32_t counter_rev =0;
uint32_t counter_fwd =0;


bool reverse_override(CONTROL_t* control){
    if(control->duty > 40 && control->spd_cmp10ms == 0){
//        control->fwd_flag=false;
//        control->fwd=false;
//        set_duty_reverse(PWM_freq, 0);
//        set_duty_fwd(PWM_freq,0);
//  printf("In the loop!!");
        control->rvrs_flag_override=0xFF;
        set_LED_Light(3);
        return true;
    }
    else{
        control->rvrs_flag_override=0x00;
        reset_LED_Light(3);
        return false;
    }

}

void chk_motor_cmd(CONTROL_t *control){

    led_display(control->duty);

    if(control->fwd){
        counter_fwd++;
        if(counter_fwd<=10){
            control->rev =false;
            set_duty_reverse(PWM_freq, 0);
            control->duty=0;
            counter_rev=0;
        }
        if(counter_fwd>10){
            set_duty_reverse(PWM_freq, 0);
            set_duty_fwd(PWM_freq,control->duty);
            led_display(control->duty);

        }
    }

    else if(control->rev){

            counter_rev++;
            if(counter_rev<=10){
                control->fwd =false;
                set_duty_fwd(PWM_freq, 0);
                control->duty=0;
                counter_fwd=0;

            }
            if(counter_rev>10){
                set_duty_fwd(PWM_freq, 0);
                set_duty_reverse(PWM_freq,control->duty);
                led_display(control->duty);

            }
        }

//        }
//        else if(control->target_speed>0){
//            set_duty_reverse(PWM_freq, 0);
//            control->fwd_flag=true;
//            control->rev=false;
//            counter_rev = 0;
//           // printf("Target speed set to be positive \n");
//        }
//    }

    else{
        set_duty_reverse(PWM_freq, 0);
        set_duty_fwd(PWM_freq,0);
        led_display(0);
        control->duty =0;
      //  printf("Zero (in else condition) \n");
        //led_display(control->duty);
    }
}



void test_motor_cmd(CONTROL_t *control){
    if(get_sw(1)){
        control->rev=true;
        printf("rev test: control->rev:- %d\n",control->rev);
        //control->fwd=false;
    }else if (get_sw(2)){
        printf("stop test \n");
        control->rev=false;
        control->fwd=false;
    }else if (get_sw(3)){
        control->fwd=true;
        printf("fwd test: control->fwd:- %d\n",control->fwd);
        //control->rev=false;

    }

}

void speed_controller(CONTROL_t *control){
    if(control->duty >80){
        control->duty=80;
    }
    if(control->duty <20 ){
        control->duty =20;
    }
    if(control->fwd){
        calls++;
        if(calls%20==0){
            if(control->spd_cmp10ms<control->target_speed/4)
            {
                control->duty++;
            }
            if(control->spd_cmp10ms>control->target_speed/4) {
                control->duty--;
            }
        }
    }
    if(control->rev){
        calls++;
        if(calls%20==0){
            if(control->spd_cmp10ms<abs(control->target_speed/4))
            {
                control->duty++;
            }
            if(control->spd_cmp10ms>abs(control->target_speed/4)) {
                control->duty--;
            }
        }
    }
}


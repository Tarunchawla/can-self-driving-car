#include <c_code/can_rx_tx.h>
#include <stdint.h>
#include "can.h"
#include "../_can_dbc/generated_can.h"
#include "pwm_wrapper.h"
#include <stdio.h>
#include "utilities.h"
#include "io_wrapper.h"
#include "string.h"
#include "rpm_feedback.h"
#include "controller.h"

DEMO_STEER_DIRECTION_t steer_packet = {0};
DEMO_MOTOR_SPEED_t speed_packet = {0};

WHEEL_ENCODER_t wheel_encoder = {0};
MOTOR_DUTY_t motor_duty_cycle = {0};
SPEED_CMPS_t speed_in_cmps = {0};
ROTATIONS_PER_SECOND_t rps_data = {0};
MOTOR_FLAG_t rvrs_flag = {0};

const uint32_t DEMO_MOTOR_SPEED__MIA_MS = 3000;
const DEMO_MOTOR_SPEED_t DEMO_MOTOR_SPEED__MIA_MSG = {0};

void can_app_init_rx(void)
{
    CAN_init(can1, 100, 100, 100, 0, 0);
    CAN_bypass_filter_accept_all_msgs();
    CAN_reset_bus(can1);
    delay_ms(500);
}

bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8]){
    can_msg_t can_msg = {0};
    can_msg.msg_id = mid;
    can_msg.frame_fields.data_len = dlc;
    memcpy(can_msg.data.bytes, bytes, dlc);
    return CAN_tx(can1, &can_msg, 0);
}

void can_app_send(CONTROL_t *control){

    wheel_encoder.WHEEL_ENCODER_data = control->rps;
    dbc_encode_and_send_WHEEL_ENCODER(&wheel_encoder);

    motor_duty_cycle.MOTOR_DUTY_CYCLE = control->duty;
    dbc_encode_and_send_MOTOR_DUTY(&motor_duty_cycle);

    speed_in_cmps.SPEED_CMPS_car = control->spd_cmp10ms;
    dbc_encode_and_send_SPEED_CMPS(&speed_in_cmps);

    rps_data.ROTATIONS_PER_SECOND_WHEELS = control->rps;
    dbc_encode_and_send_ROTATIONS_PER_SECOND(&rps_data);

    rvrs_flag.REVERSE_FLAG = control->rvrs_flag_override;
    dbc_encode_and_send_MOTOR_FLAG(&rvrs_flag);

}

void can_app_recieve_rx(CONTROL_t *control)
{
    can_msg_t msge_container = {0};


    while (CAN_rx(can1, &msge_container, 0))
    {
        dbc_msg_hdr_t can_msg_hdr = {0};
        can_msg_hdr.dlc = msge_container.frame_fields.data_len;
        can_msg_hdr.mid = msge_container.msg_id;


        if(dbc_decode_DEMO_MOTOR_SPEED(&speed_packet, msge_container.data.bytes, &can_msg_hdr)){
                    control->target_speed=speed_packet.MOTOR_SPEED_DUTY_CYCLE;
                    //printf("can speed val: %d\n",speed_packet.MOTOR_SPEED_DUTY_CYCLE);
                    if(speed_packet.MOTOR_SPEED_DUTY_CYCLE<0){
                        control->rev=true;
                        control->fwd=false;
                    }
                    else if(speed_packet.MOTOR_SPEED_DUTY_CYCLE>0){
                        if (control->fwd_flag){
                        control->fwd=true;
                        control->rev=false;
                        }
                    }
                    else if (speed_packet.MOTOR_SPEED_DUTY_CYCLE==0){
                        control->fwd=false;
                        control->rev=false;
                    }
                }
    }
    if(dbc_handle_mia_DEMO_MOTOR_SPEED(&speed_packet,10)){
        set_LED_Light(4);
    }
}

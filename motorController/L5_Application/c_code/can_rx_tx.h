
#ifndef CAN_APP_H_
#define CAN_APP_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "can.h"
typedef struct{
    int16_t target_speed;
    uint8_t duty;
    float spd_cmps;
    float spd_cmp10ms;
    float rps;
    bool fwd;
    bool rev;
    bool fwd_flag;
    uint8_t rvrs_flag_override;
}CONTROL_t;

void can_app_init_rx(void);
void can_app_recieve_rx(CONTROL_t *control);
void can_app_send(CONTROL_t *control);


#endif /* CAN_APP_H_ */
#ifdef __cplusplus
}
#endif

#include "PWM.h"
#include<lpc17xx.h>
void LabPwm::PwmSelectAllPins()
{
    LPC_SC->PCONP |=(1<<6); // Optimize this part
    LPC_PINCON->PINSEL4 |= (1 << 0);
    LPC_PINCON->PINSEL4 |= (1 << 2);
    LPC_PINCON->PINSEL4 |= (1 << 4);
    LPC_PINCON->PINSEL4 |= (1 << 6);
    LPC_PINCON->PINSEL4 |= (1 << 8);
    LPC_PINCON->PINSEL4 |= (1 << 10);
}

void LabPwm::PwmSelectPin(PWM_PIN pwm_pin_arg)
{
    LPC_SC->PCONP |=(1<<6);
    switch(pwm_pin_arg)
        {
            case k2_0 :
                LPC_PINCON->PINSEL4 |= (1<<0);
                break;

            case k2_1:
                LPC_PINCON->PINSEL4 |= (1<<2);
                break;

            case k2_2:
                LPC_PINCON->PINSEL4 |= (1<<4);
                break;

            case k2_3:
                LPC_PINCON->PINSEL4 |= (1<<6);
                break;

            case k2_4:
                LPC_PINCON->PINSEL4 |= (1<<8);
                break;

            case k2_5:
                LPC_PINCON->PINSEL4 |= (1<<10);
                break;

            default:
                break;
        }
//    LPC_PINCON->PINSEL4 &= ~(1 << (pwm_pin_arg*2-2));
}
void LabPwm::PwmInitSingleEdgeMode(uint32_t frequency_hz)
{

 //   LPC_SC->PCLKSEL0 |=(1<<13); //Enabling Clock for PWM
    LPC_PWM1->PCR &= ~(127);
//    LPC_PWM1->PCR &=~(31 << 2); //Enabling for single edge
    LPC_PWM1->PR = 23; //Setting value to 23 to get 1Mhz per tick // no needed
    LPC_PWM1->MCR |=(1 << 1); //Reset PWMTC when PWMMR0 matches TC
    LPC_PWM1->MR0 = (1000000/frequency_hz); //Sampling the tick to desired frequency TC=(SYSCLK/4)/(PR+1)=(96M/4)/(23+1)=1M
    LPC_PWM1->LER |= (1<<0); // Load the PWM to new freq tick value on next reset clock
    LPC_PWM1->PCR |= (1<<9);
    LPC_PWM1->PCR |= (32256<<0);
//    LPC_PWM1->PCR |=(63<<9);
    LPC_PWM1->TCR |= (9<<0);
}
void LabPwm::SetDutyCycle(PWM_PIN pwm_pin_arg, float duty_cycle_percentage)
{
    float duty_cycle=duty_cycle_percentage/(float)100;
    float data=(LPC_PWM1->MR0)*duty_cycle;
    switch(pwm_pin_arg)
    {
        case k2_0:
            LPC_PWM1->MR1 = data;
            LPC_PWM1->LER |= (1<<(pwm_pin_arg+1));
            break;
        case k2_1:
                    LPC_PWM1->MR2 = data;
                    LPC_PWM1->LER |= (1<<(pwm_pin_arg+1));
                    break;
        case k2_2:
                    LPC_PWM1->MR3 = data;
                    LPC_PWM1->LER |= (1<<(pwm_pin_arg+1));
                    break;
        case k2_3:
                    LPC_PWM1->MR4 = data;
                    LPC_PWM1->LER |= (1<<(pwm_pin_arg+1));
                    break;
        case k2_4:
                    LPC_PWM1->MR5 = data;
                    LPC_PWM1->LER |= (1<<(pwm_pin_arg+1));
                    break;
        case k2_5:
                    LPC_PWM1->MR6 = data;
                    LPC_PWM1->LER |= (1<<(pwm_pin_arg+1));
                    break;
        default:
            break;
    }
}

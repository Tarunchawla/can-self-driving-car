/**
 * @file
 *
 * The purpose of this "C" callbacks is to provide the code to be able
 * to call pure C functions and unit-test it in C test framework
 */
#include <c_code/can_rx_tx.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include "pwm_wrapper.h"
#include "can.h"
#include "io_wrapper.h"
#include "c_period_callbacks.h"
#include "utilities.h"
#include "rpm_feedback.h"
#include "controller.h"

CONTROL_t control = {0};

bool C_period_init(void) {
    pwm_init_motor();
    feedback_init();
    can_app_init_rx();
    set_duty_fwd(4000,0);
    set_duty_reverse(4000,0);
    control.fwd_flag=true;
    return true;
}

bool C_period_reg_tlm(void) {
    return true;
}

void C_period_1Hz(uint32_t count) {
    (void) count;
    get_spd_cm_per_sec(&control);
    printf("control->spd_cmp10ms = %f\n",control.spd_cmp10ms);

}

void C_period_10Hz(uint32_t count) {
    (void) count;
    reverse_override(&control);
    can_app_recieve_rx(&control);
    chk_motor_cmd(&control);
    can_app_send(&control);
       //test_motor_cmd(&control);

}

void C_period_100Hz(uint32_t count) {
    (void) count;
    get_spd_cm_ten_ms(&control);
    speed_controller(&control);
}

void C_period_1000Hz(uint32_t count) {
    (void) count;
}

/*
 * io_wrapper.h
 *
 *  Created on: Apr 21, 2019
 *      Author: tarun
 */

#ifndef IO_WRAPPER_H_
#define IO_WRAPPER_H_
#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

void set_as_output();
void set_high();
void set_low();
void set_LED_display(char);
void set_as_input(void);
bool read_io(void);
void led_display(int);
bool get_sw(int num);
void set_LED_Light(uint8_t);
void reset_LED_Light(uint8_t);
#ifdef __cplusplus
}
#endif



#endif /* IO_WRAPPER_H_ */

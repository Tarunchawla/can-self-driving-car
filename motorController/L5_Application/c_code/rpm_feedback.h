
#ifndef RPM_FEEDBACK_H_
#define RPM_FEEDBACK_H_

#include <c_code/can_rx_tx.h>
#include "can.h"

void feedback_init();
void get_spd_cm_ten_ms(CONTROL_t *control);
void get_spd_cm_per_sec(CONTROL_t *control);

#endif /* RPM_FEEDBACK_H_ */

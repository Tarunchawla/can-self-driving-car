/*
 * io_wrapper.cpp
 *
 *  Created on: Apr 14, 2019
 *      Author: tarun
 */

#include "io_wrapper.h"
#include "L4_IO/io.hpp"
#include "LED_Display.hpp"
#include "LED.hpp"
#include "gpio.hpp"
using namespace std;

GPIO myPin(P2_6);
GPIO myPingpio(P2_1);

void set_LED_display(char duty_cycle){
    LD.setNumber(duty_cycle);
}
void set_as_input(void){
    myPin.setAsInput();
}

bool read_io(void){
    return myPin.read();
}

void set_as_output(){
    myPingpio.setAsOutput();
}

void set_high(){
    myPingpio.setHigh();
}
void set_low(){
    myPingpio.setLow();
}
void led_display(int number){
    LD.setNumber(number);
}
bool get_sw(int num){
    bool status=SW.getSwitch(num);
    return status;
}

void set_LED_Light(uint8_t num){
LE.on(num);
}

void reset_LED_Light(uint8_t num){
LE.off(num);
}



#ifndef PWM_WRAPPER_H_
#define PWM_WRAPPER_H_
#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

typedef enum {
            pwm1=0, ///< P2.0
            pwm2=1, ///< P2.1
            pwm3=2, ///< P2.2
            pwm4=3, ///< P2.3
            pwm5=4, ///< P2.4
            pwm6=5  ///< P2.5
        } pwmType;

bool pwm_init_motor();
bool set_duty_fwd(uint32_t frequency,int duty);
bool set_duty_reverse(uint32_t frequency,int duty);
#ifdef __cplusplus
}
#endif

#endif /* PWM_WRAPPER_H_ */

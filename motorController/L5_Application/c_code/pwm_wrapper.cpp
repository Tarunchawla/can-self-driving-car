
#include "pwm_wrapper.h"
#include "L2_Drivers/lpc_pwm.hpp"
#include "pwm.h"
#include "stdio.h"
#include "stdint.h"
#include <iostream>
#include "io.hpp"

using namespace std;
//LabPwm objectpwm;
PWM preet(PWM::pwm2,400);
PWM preet2(PWM::pwm3,400);


bool pwm_init_motor(){
    //objectpwm.PwmSelectAllPins(); //Select all PWM pins
    //objectpwm.PwmSelectPin(LabPwm::k2_1); // Initialize pins to get the output
    //objectpwm.PwmSelectPin(LabPwm::k2_2);
    preet.set(0);

    return true;
}

bool set_duty_fwd(uint32_t frequency,int duty){

    //objectpwm.PwmInitSingleEdgeMode(frequency); //Initiate for Single Edge mode
    //objectpwm.SetDutyCycle((LabPwm::k2_1), duty);
    if(duty>0){
        LE.set(2, true);
    }
    else{
        LE.set(2, false);
    }
    preet.set(duty);
    return true;
}

bool set_duty_reverse(uint32_t frequency,int duty){
    //objectpwm.PwmInitSingleEdgeMode(frequency); //Initiate for Single Edge mode
    //objectpwm.SetDutyCycle((LabPwm::k2_2), duty);
    if(duty>0){
            LE.set(1, true);
        }
    else{
            LE.set(1, false);
        }
    preet2.set(duty);
    return true;
}

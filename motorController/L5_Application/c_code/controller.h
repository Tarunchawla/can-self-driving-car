/*
 * controller.h
 *
 *  Created on: Apr 26, 2019
 *      Author: Tarun
 */

#ifndef CONTROLLER_H_
#define CONTROLLER_H_
#include "can_rx_tx.h"
void chk_motor_cmd(CONTROL_t *control);
void chk_rev_cmd(CONTROL_t *control);
void speed_controller(CONTROL_t *control);
void test_motor_cmd(CONTROL_t *control);
void duty_thresholds();
bool reverse_override(CONTROL_t* control);


#endif /* CONTROLLER_H_ */

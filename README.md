# MysteryMachine

CMPE 243 Spring 2018 Project by Team "Mystery Machine"

Team Members:
Chithambaram S P
Neeraj Dhavale
Sanket Patil
Sudarshan Aithal
Adithya Baskaran
Rachit Mathur
Sai kiran
Tarun Chawla

For unit testing:
Copy the "install_mingw" "install_ruby25" "install_unit_cmock" files from
"https://sjsu.instructure.com/files/52944167/download?download_frd=1" to unit_test

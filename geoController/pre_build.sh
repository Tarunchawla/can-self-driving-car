#!/bin/bash

# 
# This file executes from the build directory by the Eclipse "Pre Build" step
#

#python ../_can_dbc/dbc_parse.py -i ../_can_dbc/243.dbc -s LIGHT > ../_can_dbc/generated_can.h
#python ../_can_dbc/dbc_parse.py -i ../_can_dbc/243.dbc -s MOTOR > ../_can_dbc/generated_can.h
python ../_can_dbc/dbc_parse.py -i ../_can_dbc/243.dbc -s GEO > ../_can_dbc/generated_can.h
#python ../_can_dbc/dbc_parse.py -i ../_can_dbc/243.dbc -s SENSOR > ../_can_dbc/generated_can.h
#python ../_can_dbc/dbc_parse.py -i ../_can_dbc/243.dbc -s BRIDGE > ../_can_dbc/generated_can.h
#python ../_can_dbc/dbc_parse.py -i ../_can_dbc/243.dbc -s MASTER > ../_can_dbc/generated_can.h
#include "unity.h" // Single Unity Test Framework include
#include <stdio.h>

#include "c_period_callbacks.h"
#include "Mockc_uart2.h"


void test_C_period_init(void) {
    TEST_ASSERT_TRUE(C_period_init());
}

void test_C_period_1Hz(void) {
    uart2_getchar_ExpectAndReturn(NULL, 0, true);
    uart2_getchar_IgnoreArg_byte();
    uint32_t a = 0;
    C_period_1Hz(a);
}

void test_C_period_10Hz(void) {
    C_period_10Hz(0);
}

void test_C_period_100Hz(void) {
    C_period_100Hz(0);
}

void testC_period_1000Hz(void) {
    C_period_1000Hz(0);
}

#include "ADC.h"

void LabAdc::AdcInitBurstMode(void)
{
    LPC_SC->PCONP |=(1<<12); //Configuring ADC
        LPC_ADC->ADCR |= (1 << 21); //Powering up the ADC
     //   LPC_SC->PCLKSEL0 |=(1<<25); //Enabling Clock for ADC
        LPC_ADC->ADCR |=(2<<8); // Dividing the clock cycles to match the condition of < 13M
        LPC_ADC->ADCR &= ~(7<<24);  //start bits of ADC = 000
        /* 2) Select ADC channel 2
        LPC_ADC->ADCR |= (1 << 2); //Choosing the channel*/
        LPC_ADC->ADCR |= (1 << 16); //Enabling the Burst mode
}

void LabAdc::AdcSelectPin(Pin pin)
{
    switch(pin)
    {
        case k0_25:
            LPC_ADC->ADCR |= (1<<2); // Setting channel 2
            LPC_PINCON->PINMODE1 |=(1 << 19); //Setting mode for neither pull up nor pull down
            LPC_PINCON->PINSEL1 |=(1 << 18); // for AD0.2
            break;
        case k0_26:
                    LPC_ADC->ADCR |= (1<<3); // Setting channel 3
                    LPC_PINCON->PINMODE1 |= (1 << 21); //Setting mode for neither pull up nor pull down
                    LPC_PINCON->PINSEL1 |=(1 << 20); // for AD0.3
                    break;
        case k1_30:
                    LPC_ADC->ADCR |= (1<<4); // Setting channel 4;
                    LPC_PINCON->PINMODE3 |= (1 << 29); //Setting mode for neither pull up nor pull down
                    LPC_PINCON->PINSEL3 |=(3 << 28); // for AD0.4
                    break;
        case k1_31:
                    LPC_ADC->ADCR |= (1<<5); // Setting channel 5;
                    LPC_PINCON->PINMODE3 |= (1 << 31); //Setting mode for neither pull up nor pull down
                    LPC_PINCON->PINSEL3 |=(3 << 30); // for AD0.5
                    break;
    }
  /*  if(pin == 25 || pin == 26)
    {
    LPC_PINCON->PINSEL1 |=(1 << pin);
    LPC_PINCON->PINMODE2 &= ~(3 << (pin - 7)); //
            LPC_PINCON->PINMODE_OD0 |= (0x1 << pin); //
    }
    else if(pin == 30 || pin == 31)
        {
        LPC_PINCON->PINSEL3 |=(1 << pin);
        LPC_PINCON->PINMODE2 &= ~(3 << (pin - 2)); //
                LPC_PINCON->PINMODE_OD1 |= (0x1 << pin); //
        }*/

}

float LabAdc::ReadAdcVoltageByChannel(uint8_t channel)
{
    uint16_t data=0;
    float voltage;
    switch(channel)
    {
        case 2:
                    data=LPC_ADC->ADDR2;//Choosing channel 2 data register
                    data=(data >> 4)&(0xFFF); // Only 12 bits are information bits and clearing other bits
                    break;
        case 3:
                    data=LPC_ADC->ADDR3;//Choosing channel 3 data register
                    data=(data >> 4)&(0xFFF); // Only 12 bits are information bits and clearing other bits
                    break;
        case 4:
                    data=LPC_ADC->ADDR4;//Choosing channel 4 data register
                    data=(data >> 4)&(0xFFF); // Only 12 bits are information bits and clearing other bits
                    break;
        case 5:
                    data=LPC_ADC->ADDR5;//Choosing channel 5 data register
                    data=(data >> 4)&(0xFFF); // Only 12 bits are information bits and clearing other bits
                    break;
        default:
            break;

    }
    voltage=(data)*(3.3/4096);//This returns the voltage converted value
    return voltage;
}

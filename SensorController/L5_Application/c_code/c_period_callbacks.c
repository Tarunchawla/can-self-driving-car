#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "ultrasonic_sensor.h"
#include "c_period_callbacks.h"
#include "ADC_wrapper.h"
#include "c_gpio.h"
#include "utilities.h"
#include "lpc_timers.h"
#include "can_bus.h"
#include "../_can_dbc/generated_can.h"
ULTRASONIC_SENSOR_VALUES ultrasonic_values = {0};
DEBUG_FLAG flags;
bool C_period_init(void) {
    init_can();
    ultrasonic_sensor_init();
    lpc_timer_enable(lpc_timer2, 1);
    lpc_timer_set_value(lpc_timer2,0);
    debug_can_c_period_init(true);
    return true;
}

bool C_period_reg_tlm(void) {
    return true;
}

void C_period_1Hz(uint32_t count) {
    (void) count;
}
void C_period_10Hz(uint32_t count) {
    (void) count;
//            uint32_t start_time = lpc_timer_get_value(lpc_timer2);
//            ultrasonic_values = ultrasonic_sensor_run();
//            debug_can_sensor_calc_total_time(lpc_timer_get_value(lpc_timer2)-start_time);
//            flags=debug_flag_updates();
//            dbc_send_sensor_msg(&ultrasonic_values,&flags);

}

void C_period_100Hz(uint32_t count) {
    (void) count;
    if(count%5 == 0){
        uint32_t start_time = lpc_timer_get_value(lpc_timer2);
        printf("start_timr\n");
        ultrasonic_values = ultrasonic_sensor_run();
        printf("ul_values\n");
        debug_can_sensor_calc_total_time(lpc_timer_get_value(lpc_timer2)-start_time);
        printf("debug_value\n");
        flags=debug_flag_updates();
        printf("flag\n");
        dbc_send_sensor_msg(&ultrasonic_values,&flags);
        printf("20 Hz\n");
   }
}

void C_period_1000Hz(uint32_t count) {
    (void) count;
}

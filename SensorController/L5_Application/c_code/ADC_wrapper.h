/*
 * ADC_wrapper.h
 *
 *  Created on: Apr 11, 2019
 *      Author: sdrsh
 */

#ifndef ADC_WRAPPER_H_
#define ADC_WRAPPER_H_
#ifdef __cplusplus
extern "C" {
#endif

typedef enum sel_Pin
    {
        k0_25,       // AD0.2 <-- Light Sensor -->
        k0_26,       // AD0.3
        k1_30,       // AD0.4
        k1_31,       // AD0.5

        /* These ADC channels are compromised on the SJ-One,
         * hence you do not need to support them
         */
        // k0_23 = 0,   // AD0.0
        // k0_24,       // AD0.1
        // k0_3,        // AD0.6
        // k0_2         // AD0.7
    }Pin;

void AdcInitBurst(void);
void AdcSelectPin(Pin pin);
float ReadAdcVoltageByChannel(uint8_t channel);


#ifdef __cplusplus
}
#endif
#endif /* ADC_WRAPPER_H_ */

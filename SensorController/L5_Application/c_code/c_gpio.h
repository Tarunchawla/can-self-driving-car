/*
 * c_gpio.h
 *
 *  Created on: Mar 4, 2019
 *      Author: zspcc
 */

#ifndef C_GPIO_H_
#define C_GPIO_H_

#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>
#include <stdbool.h>
#define ECHO_LEFT 0
#define ECHO_CENTER 1
#define ECHO_RIGHT 2


void set_trig_output(void);
void set_echo_input(void);
void set_trigger_as_high(void);
void set_trigger_as_low(void);
bool check_echo(uint8_t sensor, bool value);
bool read_echo(uint8_t x);


#ifdef __cplusplus
}
#endif

#endif /* C_GPIO_H_ */

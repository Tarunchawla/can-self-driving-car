
#include "c_gpio.h"
#include "lpc_timers.h"
#include "ultrasonic_sensor.h"
#include "stdbool.h"
#define TIMEOUT 6

ULTRASONIC_SENSOR_VALUES ultrasonic_sensor_values;
bool left, center, right;
uint32_t left_start_time, right_start_time, center_start_time;
DEBUG_FLAG flags;

void ultrasonic_sensor_reset_values(){
    ultrasonic_sensor_values.center_sensor = 0;
    ultrasonic_sensor_values.left_sensor = 0;
    ultrasonic_sensor_values.right_sensor = 0;
    left_start_time = right_start_time = center_start_time = 0;
}
void ultrasonic_sensor_init(){

    set_echo_input();
    set_trig_output();
    lpc_timer_enable(lpc_timer0, 1);
    lpc_timer_enable(lpc_timer3, 1000);
    lpc_timer_set_value(lpc_timer0,0);
    lpc_timer_set_value(lpc_timer3,0);
    debug_sensor_init(true);
}

void ultrasonic_sensor_trigger(void){
    set_trigger_as_low();
    delay_us(2);
    set_trigger_as_high();
    delay_us(10);
    set_trigger_as_low();
    debug_sensor_trigger (true);
}


void ultrasonics_sensor_calc_distance(){
    left = center = right = true;
    flags.left_done = flags.right_done = flags.center_done = false;
    uint32_t start_time = lpc_timer_get_value(lpc_timer3);
    while(!flags.left_done || !flags.center_done || !flags.right_done){
        if(check_echo(ECHO_LEFT, left))
            ultrasonic_sensor_calc_time(ECHO_LEFT, left);
        if(check_echo(ECHO_CENTER, center))
            ultrasonic_sensor_calc_time(ECHO_CENTER, center);
        if(check_echo(ECHO_RIGHT, right))
            ultrasonic_sensor_calc_time(ECHO_RIGHT, right);
        if((lpc_timer_get_value(lpc_timer3) - start_time) > TIMEOUT){
            ultrasonic_sensor_values.timeout = true;
            printf("Timeout");
            if(!flags.left_done) ultrasonic_sensor_values.left_sensor = 300;
            if(!flags.right_done) ultrasonic_sensor_values.right_sensor = 300;
            if(!flags.center_done) ultrasonic_sensor_values.center_sensor = 300;
            break;
        }
    }
}

void ultrasonic_sensor_calc_time(uint8_t sensor, bool pin_value){
    switch(sensor){
        case ECHO_LEFT:
            ultrasonic_sensor_left_proc(pin_value);
            break;
        case ECHO_CENTER:
            ultrasonic_sensor_center_proc(pin_value);
            break;
        case ECHO_RIGHT:
            ultrasonic_sensor_right_proc(pin_value);
            break;
    }
}
void ultrasonic_sensor_left_proc(bool pin_value){
    if(pin_value){
        left_start_time = lpc_timer_get_value(lpc_timer0);
        left = false;
    } else if(!flags.left_done){
        uint32_t one_trip_time = (lpc_timer_get_value(lpc_timer0) - left_start_time)/2;
        ultrasonic_sensor_values.left_sensor = (0.0343 * (one_trip_time));
        flags.left_done = true;
        debug_sensor_left_one_trip_time(one_trip_time);
      //  printf("done left, time in us:%d\n", one_trip_time);
    }
}

void ultrasonic_sensor_center_proc(bool pin_value){
    if(pin_value){
        center_start_time = lpc_timer_get_value(lpc_timer0);
        center = false;
    } else if(!flags.center_done){
        uint32_t one_trip_time = (lpc_timer_get_value(lpc_timer0) - center_start_time)/2;
        ultrasonic_sensor_values.center_sensor = (0.0343 * (one_trip_time));
        flags.center_done = true;
        debug_sensor_center_one_trip_time(one_trip_time);
        //printf("done center, time in us: %d\n", one_trip_time);
    }
}

void ultrasonic_sensor_right_proc(bool pin_value){
    if(pin_value){
        right_start_time = lpc_timer_get_value(lpc_timer0);
        right = false;
    } else if(!flags.right_done){
        uint32_t one_trip_time = (lpc_timer_get_value(lpc_timer0) - right_start_time)/2;
        ultrasonic_sensor_values.right_sensor = (0.0343 * (one_trip_time));
        flags.right_done = true;
        debug_sensor_right_one_trip_time(one_trip_time);
        printf("done right, time in us: %d\n",one_trip_time);
    }
}

ULTRASONIC_SENSOR_VALUES ultrasonic_sensor_run(){
    ultrasonic_sensor_reset_values();
    ultrasonic_sensor_trigger();
    ultrasonics_sensor_calc_distance();
    printf("left = %d, center = %d, right = %d\n",
            ultrasonic_sensor_values.left_sensor,
            ultrasonic_sensor_values.center_sensor,
            ultrasonic_sensor_values.right_sensor);
    return ultrasonic_sensor_values;
    /*
    distance_L = (0.0343 * get_echo(0))/2;
    l_avg+=distance_L;
    ultrasonic_sensor_trigger();
    distance_C = (0.0343 * get_echo(1))/2;
    c_avg+=distance_C;
    ultrasonic_sensor_trigger();
    distance_R = (0.0343 * get_echo(2))/2;
    r_avg+=distance_R;
    delay_us(10);
    */
}
DEBUG_FLAG debug_flag_updates(){
    return flags;
}

/*
 * gpio_wrapper.cpp
 *
 *  Created on: Mar 4, 2019
 *      Author: sdrsh
 */
#include "gpio.hpp"
#include "c_gpio.h"
#include "stdio.h"


GPIO trigger(P2_1);
GPIO echo_L(P2_4);
GPIO echo_C(P2_3);
GPIO echo_R(P2_2);

void set_trig_output(){
    trigger.setAsOutput();
}

void set_echo_input(){
    echo_L.setAsInput();
    echo_C.setAsInput();
    echo_R.setAsInput();
}

void set_trigger_as_high(){
    trigger.setHigh();
}

void set_trigger_as_low(){
    trigger.setLow();
}

bool check_echo(uint8_t sensor, bool value) {
    bool out;
    switch(sensor){
        case ECHO_LEFT:
            out = (value == echo_L.read())? true: false;
            break;
        case ECHO_CENTER:
            out = (value == echo_C.read())? true: false;
            break;
        case ECHO_RIGHT:
            out = (value == echo_R.read())? true: false;
            break;
        default:
            out = false;
            break;
    }
    return out;
}

bool read_echo(uint8_t x){
    bool read;
    switch(x){
        case 0:
            read=echo_L.read();
            break;
        case 1:
            read=echo_C.read();
            break;
        case 2:
            read=echo_R.read();
            break;
        default:
            printf("ECHO NOT SELECTED");
    }
    return read;
}


#ifndef ULTRASONIC_SENSOR_H_
#define ULTRASONIC_SENSOR_H_
#include <stdbool.h>
#define ECHO_LEFT 0
#define ECHO_CENTER 1
#define ECHO_RIGHT 2

typedef struct{
    uint16_t left_sensor;
    uint16_t center_sensor;
    uint16_t right_sensor;
    bool timeout;
}ULTRASONIC_SENSOR_VALUES;
typedef struct{
    bool  left_done;
    bool  right_done;
    bool  center_done;
}DEBUG_FLAG;
void ultrasonic_sensor_init();
void ultrasonic_sensor_trigger();
void ultrasonics_sensor_calc_distance();
void ultrasonic_sensor_calc_time(uint8_t sensor, bool pin_value);
void ultrasonic_sensor_left_proc(bool pin_value);
void ultrasonic_sensor_center_proc(bool pin_value);
void ultrasonic_sensor_right_proc(bool pin_value);
ULTRASONIC_SENSOR_VALUES ultrasonic_sensor_run();
DEBUG_FLAG debug_flag_updates();


#endif /* ULTRASONIC_SENSOR_H_ */

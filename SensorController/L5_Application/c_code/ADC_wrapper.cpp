/*
 * ADC_wrapper.cpp
 *
 *  Created on: Apr 11, 2019
 *      Author: sdrsh
 */
#include "ADC.h"
#include "ADC_wrapper.h"
#include <stdint.h>
LabAdc ADCobject;
void AdcInitBurst(void){
    ADCobject.AdcInitBurstMode();
}
void AdcSelectPin(Pin pin){
    ADCobject.AdcSelectPin((LabAdc::Pin)pin);
}
float ReadAdcVoltageByChannel(uint8_t channel){
    float temp = ADCobject.ReadAdcVoltageByChannel(channel);
    return temp;
}

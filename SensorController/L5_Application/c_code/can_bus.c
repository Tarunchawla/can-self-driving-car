
#include "stdint.h"
#include "can_bus.h"
#include "can.h"
#include "stdbool.h"
#include "stdlib.h"
#include "utilities.h"
#include "ultrasonic_sensor.h"
#include "../_can_dbc/generated_can.h"
#include <string.h>
#define BAUD_RATE 100
#define RXQ_SIZE 0x100
#define TXQ_SIZE 0x100
#define CAN_TIMEOUT 0x00

void bus_off(void){
    //CAN_reset_bus(can1);
}

void data_over(void){
//    printf("Data overrun\n");
}

bool init_can(){
    bool success = CAN_init(can1, BAUD_RATE, RXQ_SIZE, TXQ_SIZE, (can_void_func_t)&bus_off,(can_void_func_t)& data_over);
    printf("CAN_init\n");
    CAN_bypass_filter_accept_all_msgs();
    printf("bypass_filter\n");
    CAN_reset_bus(can1);
    printf("reset_bus\n");
    delay_ms(500);
    printf("%d\n",success);
    return success;
}

bool dbc_send_sensor_msg(ULTRASONIC_SENSOR_VALUES *sonar_msg,DEBUG_FLAG *flags){
    SONAR_SENSOR_VALUES_t sonar_sensor_values;
    sonar_sensor_values.SONAR_SENSOR_VALUES_Front_Sensor = sonar_msg->center_sensor;
    sonar_sensor_values.SONAR_SENSOR_VALUES_Left_Sensor = sonar_msg->left_sensor;
    sonar_sensor_values.SONAR_SENSOR_VALUES_Right_Sensor = sonar_msg->right_sensor;
    dbc_encode_and_send_SONAR_SENSOR_VALUES(&sonar_sensor_values);
    SENSOR_TIMEOUT_t sonar_sensor_timeout;
    if(sonar_msg->timeout){
        if(!flags->left_done){
            sonar_sensor_timeout.SENSOR_TIMEOUT_timeout=1;
        }
        else if(!flags->center_done){
            sonar_sensor_timeout.SENSOR_TIMEOUT_timeout=2;
        }
        else if(!flags->right_done){
            sonar_sensor_timeout.SENSOR_TIMEOUT_timeout=3;
        }
    }
    else
        sonar_sensor_timeout.SENSOR_TIMEOUT_timeout=0;
    dbc_encode_and_send_SENSOR_TIMEOUT(&sonar_sensor_timeout);
    return true;
}
bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8])
{
    can_msg_t can_msg = { 0 };
    can_msg.msg_id                = mid;
    can_msg.frame_fields.data_len = dlc;
    memcpy(can_msg.data.bytes, bytes, dlc);
    //printf("dbc_app_send_can_msg: sending in can\n");
    return CAN_tx(can1, &can_msg, 0);
}


bool can_send(float left,float right,float center){
    DEMO_IR_SENSOR_t sensor_packet = {0};
    sensor_packet.LEFT_SENSOR = left;
    sensor_packet.RIGHT_SENSOR = right;
    sensor_packet.FRONT_SENSOR = center;
//    printf("can_send: left:%f right:%f center:%f\n",left, right, center);

    // Encode the CAN message's data bytes, get its header and set the CAN message's DLC and length
    //dbc_msg_hdr_t msg_hdr = dbc_encode_MOTOR_CMD(can_msg.data.bytes, &motor_cmd);
    return dbc_encode_and_send_DEMO_IR_SENSOR(&sensor_packet);
    //printf("ACK:%d\n",ACK);
}

bool debug_can_sensor_calc_total_time(uint32_t total_time){
    TOTAL_SENSOR_CALCULATION_TIME_t sensor_total_time ={0};
    sensor_total_time.TOTAL_SENSOR_CALCULATION_TIME_total_time = total_time;
    return dbc_encode_and_send_TOTAL_SENSOR_CALCULATION_TIME(&sensor_total_time);

}

bool debug_sensor_left_one_trip_time(uint32_t one_trip_time){
    LEFT_SENSOR_ECHO_TIME_t sensor_onetrip_left_time ={0};
    sensor_onetrip_left_time.LEFT_SENSOR_ECHO_TIME_left_calculation_time = one_trip_time;
    return dbc_encode_and_send_LEFT_SENSOR_ECHO_TIME(&sensor_onetrip_left_time);
}

bool debug_sensor_center_one_trip_time(uint32_t one_trip_time){
    CENTER_SENSOR_ECHO_TIME_t sensor_onetrip_center_time = {0};
    sensor_onetrip_center_time.CENTER_SENSOR_ECHO_TIME_center_calculation_time = one_trip_time;
    return dbc_encode_and_send_CENTER_SENSOR_ECHO_TIME(&sensor_onetrip_center_time);
}

bool debug_sensor_right_one_trip_time(uint32_t one_trip_time){
    RIGHT_SENSOR_ECHO_TIME_t sensor_onetrip_right_time = {0};
    sensor_onetrip_right_time.RIGHT_SENSOR_ECHO_TIME_right_calculation_time = one_trip_time;
    return dbc_encode_and_send_RIGHT_SENSOR_ECHO_TIME(&sensor_onetrip_right_time);
}

bool debug_sensor_init(bool status){
    SENSOR_INIT_t sensor_ultrasonic_init = {0};
    sensor_ultrasonic_init.SENSOR_INIT_init_Status = status;
    return dbc_encode_and_send_SENSOR_INIT(&sensor_ultrasonic_init);
}

bool debug_sensor_trigger(bool status){
    SENSOR_TRIGGER_t sensor_ultrasonic_trigger = {0};
    sensor_ultrasonic_trigger.SENSOR_TRIGGER_Trigger_status = status;
    return dbc_encode_and_send_SENSOR_TRIGGER(&sensor_ultrasonic_trigger);
}

bool debug_can_c_period_init(bool status){
    PERIODIC_SCHEDULER_INIT_t sensor_c_periodic_init = {0};
    sensor_c_periodic_init.PERIODIC_SCHEDULER_INIT_scheduler_init_status = status;
    return dbc_encode_and_send_PERIODIC_SCHEDULER_INIT(&sensor_c_periodic_init);
}


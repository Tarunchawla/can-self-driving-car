
#ifndef CAN_BUS_H_
#define CAN_BUS_H_

#include <can.h>
#include "ultrasonic_sensor.h"
bool init_can();
bool can_send(float left,float right,float center);
bool dbc_send_sensor_msg(ULTRASONIC_SENSOR_VALUES *sonar_msg,DEBUG_FLAG *flags);
bool debug_can_sensor_calc_total_time(uint32_t total_time);
bool debug_sensor_left_one_trip_time(uint32_t one_trip_time);
bool debug_sensor_center_one_trip_time(uint32_t one_trip_time);
bool debug_sensor_right_one_trip_time(uint32_t one_trip_time);
bool debug_sensor_init(bool status);
bool debug_sensor_trigger(bool status);
bool debug_can_c_period_init(bool status);
#endif /* CAN_BUS_H_ */
